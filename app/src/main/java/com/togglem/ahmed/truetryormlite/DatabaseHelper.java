package com.togglem.ahmed.truetryormlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by ahmed on 8/26/2014.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DB_NAME = "7mo";
    private static final int databaseVersion = 2;


    public RuntimeExceptionDao<Student, Integer> Students = getRuntimeExceptionDao(Student.class);
    public RuntimeExceptionDao<Teacher, Integer> Teachers  = getRuntimeExceptionDao(Teacher.class);
    public RuntimeExceptionDao<Course, Integer> Courses = getRuntimeExceptionDao(Course.class);
    public RuntimeExceptionDao<StudentsCourses, Integer> StudentsCourses = getRuntimeExceptionDao(StudentsCourses.class);


    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, databaseVersion);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {

            TableUtils.createTable(connectionSource, Student.class);
            TableUtils.createTable(connectionSource, Course.class);
            TableUtils.createTable(connectionSource, Teacher.class);
            TableUtils.createTable(connectionSource, StudentsCourses.class);

            Log.d("DBDBDB", " :D  done creating DB");

        } catch (SQLException e) {

            Log.d("DBDBDB", " :'( not done creating DB");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i2) {
        try {
            TableUtils.dropTable(connectionSource, Student.class, true);
            TableUtils.dropTable(connectionSource, Course.class, true);
            TableUtils.dropTable(connectionSource, Teacher.class, true);
            TableUtils.dropTable(connectionSource, StudentsCourses.class, true);
            this.onCreate(sqLiteDatabase, connectionSource);

            Log.d("DBDBDB", " :D I have Updated the DB");
        } catch (SQLException e) {


            Log.d("DBDBDB", " :'( I have not Updated the DB");
        }

    }
}
