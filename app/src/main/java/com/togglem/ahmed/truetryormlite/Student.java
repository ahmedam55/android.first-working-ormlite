package com.togglem.ahmed.truetryormlite;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by ahmed on 8/26/2014.
 */
public class Student {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    public String name;
    @DatabaseField
    public int age;
    @DatabaseField
    public int grade;
    @DatabaseField(foreign = true,foreignAutoRefresh = true)
    public Teacher teacher;

    public Student() {
    }

    public Student(String name, int age, int grade) {
        this.name = name;
        this.age = age;
        this.grade = grade;
    }

}


