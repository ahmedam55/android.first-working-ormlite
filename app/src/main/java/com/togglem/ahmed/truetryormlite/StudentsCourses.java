package com.togglem.ahmed.truetryormlite;

import com.j256.ormlite.field.DatabaseField;


public class StudentsCourses {
    @DatabaseField(foreign = true)
    public Student student;
    @DatabaseField(foreign = true)
    public Course course;

    public StudentsCourses() {
    }

    public StudentsCourses(Student student,Course course) {

        this.student=student;
        this.course=course;
    }

}
