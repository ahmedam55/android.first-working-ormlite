package com.togglem.ahmed.truetryormlite;

import com.j256.ormlite.field.DatabaseField;


public class Course {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    public String name;
    @DatabaseField(foreign = true,foreignAutoRefresh = true)
    public Teacher teacher;


    public Course() {
    }

    public Course(String name) {
        this.name=name;
    }
}
