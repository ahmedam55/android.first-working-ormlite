package com.togglem.ahmed.truetryormlite;

import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.List;


public class MyActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        DatabaseHelper DB = new DatabaseHelper(getApplicationContext());


        LinearLayout lili= (LinearLayout) findViewById(R.id.linearLLL);

        List<Student> studentList=DB.Students.queryForAll();

        for (int i = 0; i < studentList.size(); i++) {
            TextView texty= new TextView(this);
            Student g=studentList.get(i);
            texty.setBackgroundColor(getResources().getColor(R.color.red));
            texty.setText(g.name+"__"+g.age+"__"+g.grade);
            texty.setTextAppearance(this, android.R.style.TextAppearance_Small);
            lili.addView(texty);
        }


        List<Teacher> teacherList=DB.Teachers.queryForAll();
        DB.Teachers.delete(teacherList        );

        for (int i = 0; i < teacherList.size(); i++) {
            TextView texty= new TextView(this);
            Teacher g=teacherList.get(i);
            texty.setBackgroundColor(getResources().getColor(R.color.greenWhite));
            texty.setText(g.name+"__"+g.subject);
            texty.setTextAppearance(this,android.R.style.TextAppearance_Small);
            lili.addView(texty);
        }



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }




















private void fillData(){

    DatabaseHelper DB = new DatabaseHelper(getApplicationContext());

    try {
        DB.Students.deleteBuilder().delete();
    } catch (SQLException e) {
        e.printStackTrace();
    }

    DB.Students.create(new Student("s3ad", 45, 2));
    DB.Students.create(new Student("Mahmoud", 44, 1));
    DB.Students.create(new Student("7sny", 12, 48));
    DB.Students.create(new Student("farouk", 8, 48));
    DB.Students.create(new Student("3bdo", 5, 84));

    DB.Teachers.create(new Teacher("Alaa","E"));
    DB.Teachers.create(new Teacher("safa","Arabic"));
    DB.Teachers.create(new Teacher("Mahmoud","Quran"));
    DB.Teachers.create(new Teacher("Marwa","Computer"));
    DB.Teachers.create(new Teacher("shimaa","E"));
}



}


