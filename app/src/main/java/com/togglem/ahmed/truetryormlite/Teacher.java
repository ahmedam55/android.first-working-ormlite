package com.togglem.ahmed.truetryormlite;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

/**
 * Created by ahmed on 8/26/2014.
 */
public class Teacher {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    public String name;
    @DatabaseField
    public String subject;
    @ForeignCollectionField
    public ForeignCollection<Student> students;
    @DatabaseField(foreign = true,foreignAutoRefresh = true)
    public Teacher coachedTeacher;
    @DatabaseField(foreign = true,foreignAutoRefresh = true)
    public Course teachingCourse;


    public Teacher() {
    }


    public Teacher(String name,String subject) {
        this.name=name;
        this.subject=subject;
    }
}
